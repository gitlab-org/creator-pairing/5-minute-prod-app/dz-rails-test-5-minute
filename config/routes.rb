Rails.application.routes.draw do
  resources :products
  get 'welcome/index'

  root 'welcome#index'
end
